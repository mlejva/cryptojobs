// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import firebase from 'firebase';
import 'firebase/firestore';
import VueAnalytics from 'vue-analytics';
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from '@/router/routes';
import App from './App';

/* <Setup Firebase> */
const config = {
  apiKey: 'AIzaSyA2RmeiI1NW3Fnq3WJp5cwPpd31zIHIHKQ',
  authDomain: 'cryptojobs-4e761.firebaseapp.com',
  databaseURL: 'https://cryptojobs-4e761.firebaseio.com',
  projectId: 'cryptojobs-4e761',
  storageBucket: 'cryptojobs-4e761.appspot.com',
  messagingSenderId: '877272138262',
};
firebase.initializeApp(config);
/* </Setup Firebase> */

/* <Setup app's router> */
Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode: 'history',
  /* mode: 'hash', */
});
/* </Setup app's router> */

/* <Setup Vue Analytics> */
Vue.use(VueAnalytics, {
  id: 'UA-114996637-1',
  router,
});
/* </Setup Vue Analytics> */

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
