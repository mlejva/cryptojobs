import firebase from 'firebase';

export default {
  methods: {
    async writeJob(newJob, companyLogoFile) {
      const jobRef = firebase.firestore().collection('jobs').doc();

      const jobObj = newJob;
      jobObj.timestamp = firebase.firestore.FieldValue.serverTimestamp();
      jobObj.id = jobRef.id;


      // Save company logo into a storage
      const storageRef = firebase.storage().ref();
      const logoStorageRef = storageRef.child(`jobs/${jobRef.id}/companyLogo.png`);

      const uploadTask = await logoStorageRef.put(companyLogoFile);
      jobObj.companyLogoURL = uploadTask.downloadURL;
      await jobRef.set(jobObj);
    },

    async getJobLogo(jobId) {
      const storageRef = firebase.storage().ref();
      const logoRef = storageRef.child(`jobs/${jobId}/companyLogo.png`);
      return logoRef.getDownloadURL();
    },

    async getJob(jobId) {
      const jobRef = firebase.firestore().doc(`jobs/${jobId}`);
      const snapshot = await jobRef.get();

      if (snapshot.exists) {
        return snapshot.data();
      }

      throw new Error('Job does not exist');
    },

    async getJobs(isFirstBatch, last, batchSize) {
      let query = {};
      if (isFirstBatch) {
        query = firebase.firestore().collection('jobs').orderBy('timestamp', 'desc').limit(batchSize);
      } else {
        query = firebase.firestore().collection('jobs').orderBy('timestamp', 'desc').startAfter(last)
          .limit(batchSize);
      }
      return query.get();
    },
  },
};
