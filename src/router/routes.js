import AppHome from '@/components/AppHome';

import JobSubmit from '@/components/JobSubmit';
import JobDetail from '@/components/JobDetail';

export default
[
  {
    path: '/',
    name: 'AppHome',
    component: AppHome,
  },
  {
    path: '/submit',
    name: 'JobSubmit',
    component: JobSubmit,
  },
  {
    path: '/job/:id',
    name: 'JobDetail',
    component: JobDetail,
  },
];
